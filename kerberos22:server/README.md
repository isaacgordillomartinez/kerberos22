# KERBEROS SERVER 2022/2023 INTERACTIVO
# Isaac Gordillo

Este es un contenedor de Docker para practicar con Kerberos.

## Ejecución del contenedor

```
docker build -t isaacgm22/kerberos22:server .
```

```
docker run --rm --name kserver.edt.org -h kserver.edt.org --net 2hisx -p 88:88 -p 749:749 -p 464:464 -it isaacgm22/kerberos22:server /bin/bash
```

## startup.sh

## install.sh

## **Archivos de configuración**

- Paquetes necesarios:

  * **krb5**. .

## Contenido de los archivos de configuración

- **/etc/krb5/krb5.conf**

  * Aquí hay cosas.

Usar orden updatedb y locate krb5.conf para ver que el fichero está en /etc. Hay que instalar mlocate para esto.

Dentro de krb5.conf hay esto:

```
[libdefaults]
	default_realm = EDT.ORG     # Este es un dominio, un "reino", que tiene reinados.
                              # Siempre tiene que ir en mayúsculas.

# The following krb5.conf variables are only for MIT Kerberos.
	kdc_timesync = 1
	ccache_type = 4
	forwardable = true
	proxiable = true

# The following encryption type specification will be used by MIT Kerberos
# if uncommented.  In general, the defaults in the MIT Kerberos code are
# correct and overriding these specifications only serves to disable new
# encryption types as they are added, creating interoperability problems.
#
# The only time when you might need to uncomment these lines and change
# the enctypes is if you have local software that will break on ticket
# caches containing ticket encryption types it doesn't know about (such as
# old versions of Sun Java).

#	default_tgs_enctypes = des3-hmac-sha1
#	default_tkt_enctypes = des3-hmac-sha1
#	permitted_enctypes = des3-hmac-sha1

# The following libdefaults parameters are only for Heimdal Kerberos.
	fcc-mit-ticketflags = true

[realms]
  EDT.ORG = {
    kdc = kserver.edt.org
    admin_server = kserver.edt.org
  }

[domain_realm]
  .edt.org = EDT.ORG
  edt.org = EDT.ORG
```

kerberos distribution server = kdc


```
kdb5_util create -s
```

**kadmin** es la herramienta de administración, que usa el protocolo Kerberos y se
puede usar desde cualquier host que esté configurado como cliente kerberos.

**kadmin.local** es lo mismo pero forma interactiva. Por lo que sirve para realizar
pruebas pero no para ejecutar scripts.

```
addprinc ramon
delprinc ramon
```


```
kadmin.local -q "addprinc pere"
```

- q: query
- "addprinc pere": añadir **principal** (usuario y contraseña) para pere.

Lo mismo para los usuarios **admin/admin** y **pere/admin**

```
kadmin.local -q "addprinc admin/admin"
kadmin.local -q "addprinc pere/admin"
```

Listar los principals:

```
kadmin.local -q "listprincs"
```

- **/etc/krb5kdc/kadm5.acl**:

```
kadmin.local -q "addprinc pere"
```


- Encender demonio de Kerberos:

```
krb5 service no se qué
```